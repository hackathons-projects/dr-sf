fr_current_with_titles.ipynb формирует файлы расчётов result_data_v3.json, target_data_v3.json, metrics_data_v3.json
для дальнейшено использования их в frontend модуле.
Некоторые промежуточные данные требуют продолжительных расчётов (около 6 часов ), поэтому выкладываем их отдельно
https://drive.google.com/drive/folders/1tZVT13MaJWP6pax_lLRI02S6L0NyglJF?usp=sharing
