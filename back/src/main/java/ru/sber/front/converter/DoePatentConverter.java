package ru.sber.front.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.sber.front.domain.model.DoePatentEntity;
import ru.sber.front.domain.model.LinkList;
import ru.sber.front.model.DoePatentDto;

@Transactional
@Component
public class DoePatentConverter implements Converter<DoePatentDto, DoePatentEntity> {

    @Override
    public DoePatentEntity convert(MappingContext<DoePatentDto, DoePatentEntity> mappingContext) {
        DoePatentEntity result = new DoePatentEntity();
        result.setResearchOrgs(mappingContext.getSource().getResearchOrgs());
        result.setAuthors(mappingContext.getSource().getAuthors());
        result.setLinks(new LinkList(mappingContext.getSource().getLinks()));
        result.setSponsorOrgs(mappingContext.getSource().getSponsorOrgs());
        result.setAssignee(mappingContext.getSource().getAssignee());
        result.setApplicationNumber(mappingContext.getSource().getApplicationNumber());
        result.setDescription(mappingContext.getSource().getDescription());
        result.setDoeContractNumber(mappingContext.getSource().getDoeContractNumber());
        result.setCountryPublication(mappingContext.getSource().getCountryPublication());
        result.setEntryDate(mappingContext.getSource().getEntryDate());
        result.setFormat(mappingContext.getSource().getFormat());
        result.setLanguage(mappingContext.getSource().getLanguage());
        result.setOstiId(mappingContext.getSource().getOstiId());
        result.setTitle(mappingContext.getSource().getTitle());
        result.setPatentNumber(mappingContext.getSource().getPatentNumber());
        result.setPublicationDate(mappingContext.getSource().getPublicationDate());
        result.setProductType(mappingContext.getSource().getProductType());
        return result;
    }

}
