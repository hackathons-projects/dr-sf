package ru.sber.front.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.sber.front.domain.model.ArXivArticleEntity;
import ru.sber.front.exception.ArXivParsingException;
import ru.sber.front.util.ArXivHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.Optional;

@Transactional
@Component
public class ArXivConverter implements Converter<String, ArXivArticleEntity> {

    private final SAXParser saxParser;

    public ArXivConverter() throws ParserConfigurationException, SAXException {
        saxParser = SAXParserFactory.newInstance().newSAXParser();
    }

    @Override
    public ArXivArticleEntity convert(MappingContext<String, ArXivArticleEntity> mappingContext) {
        InputSource is = new InputSource(new StringReader(Optional.ofNullable(mappingContext.getSource()).orElseThrow()));
        try {
            ArXivHandler arXivHandler = new ArXivHandler();
            saxParser.parse(is, arXivHandler);
            Optional.ofNullable(arXivHandler.getResult().getArticleId()).orElseThrow(ArXivParsingException::new);
            return arXivHandler.getResult();
        } catch (SAXException | IOException | ArXivParsingException e) {
            return null;
        }
    }

}
