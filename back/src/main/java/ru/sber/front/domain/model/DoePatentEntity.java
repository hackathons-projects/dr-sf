package ru.sber.front.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import ru.sber.front.domain.converter.AttributesConverter;
import ru.sber.front.domain.converter.LinkListConverter;

import javax.persistence.*;
import java.util.Date;

@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "doe_patents")
public class DoePatentEntity extends BaseEntity<Long> {

    @JsonProperty("osti_id")
    @Column(length = 1000)
    public String ostiId;

    @Column(length = 1000)
    public String title;

    @Column(length = 1000)
    public String productType;

    @Column(length = 1000)
    public String language;

    @Column(length = 1000)
    public String countryPublication;

    @Lob
    @Column
    public String description;

    @Column(length = 1000)
    public Date publicationDate;

    @Column
    public Date entryDate;

    @Column(length = 1000)
    public String format;

    @Column(length = 4000)
    @Convert(converter = AttributesConverter.class)
    public AttributesList authors = new AttributesList();

    @Column(length = 1000)
    public String doeContractNumber;

    @Column(length = 4000)
    @Convert(converter = AttributesConverter.class)
    public AttributesList sponsorOrgs = new AttributesList();

    @Column(length = 4000)
    @Convert(converter = AttributesConverter.class)
    public AttributesList researchOrgs = new AttributesList();

    @Column(length = 1000)
    public String patentNumber;

    @Column(length = 1000)
    public String applicationNumber;

    @Column(length = 1000)
    public String assignee;

    @Lob
    @Convert(converter = LinkListConverter.class)
    public LinkList links = new LinkList();
}
