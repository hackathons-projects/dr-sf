package ru.sber.front.domain.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import ru.sber.front.domain.model.AttributesList;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;

@Slf4j
@Converter
public class AttributesConverter implements AttributeConverter<AttributesList, String> {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(AttributesList customerInfo) {
        try {
            return objectMapper.writeValueAsString(customerInfo);
        } catch (final JsonProcessingException e) {
            log.error("JSON writing error", e);
            return null;
        }
    }

    @Override
    public AttributesList convertToEntityAttribute(String customerInfoJSON) {
        try {
            return objectMapper.readValue(customerInfoJSON, AttributesList.class);
        } catch (final IOException e) {
            log.error("JSON reading error", e);
            return null;
        }
    }

}
