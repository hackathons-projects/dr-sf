package ru.sber.front.domain.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class LinkList {

    private final List<Link> links;

    public LinkList(List<Link> links) {
        this.links = links;
    }

    public LinkList() {
        this.links = new ArrayList<>();
    }
}
