package ru.sber.front.domain.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "rffi_records")
public class RffiRecordEntity extends BaseEntity<Long> {

    @Column
    private String projectNumber;

    @Column
    private String projectCurator;

    @Column(length = 4000)
    private String title;

    @Column
    private String stage;
}
