package ru.sber.front.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.front.domain.model.ArXivArticleEntity;

public interface ArXivArticleRepository extends JpaRepository<ArXivArticleEntity, Long> {

    boolean existsByArticleId(String articleId);
}
