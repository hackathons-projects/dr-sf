package ru.sber.front.domain.model;

import lombok.*;
import ru.sber.front.domain.converter.AttributesConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "arxiv_articles")
public class ArXivArticleEntity extends BaseEntity<Long> {

    @Column
    private String articleId;

    @Column
    private LocalDateTime updated;

    @Column
    private LocalDateTime published;

    @Column
    private String title;

    @Lob
    @Column
    private String summary;

    @Lob
    @Convert(converter = AttributesConverter.class)
    private AttributesList authors = new AttributesList();

    @Column
    private String doi;

    @Lob
    @Column
    private String comment;

    @Column
    private String journal;

    @Lob
    @Convert(converter = AttributesConverter.class)
    private AttributesList categories = new AttributesList();

    @Column
    private String pdfLink;

    @Column
    private Boolean pdfSaved;
}
