package ru.sber.front.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.front.domain.model.RffiRecordEntity;

public interface RffiRecordRepository extends JpaRepository<RffiRecordEntity, Long> {
}
