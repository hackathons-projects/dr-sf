package ru.sber.front.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.front.domain.model.DoePatentEntity;

public interface DoePatentRepository extends JpaRepository<DoePatentEntity, Long> {

    boolean existsByOstiId(String ostiId);
}
