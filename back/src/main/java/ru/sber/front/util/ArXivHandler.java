package ru.sber.front.util;

import lombok.Getter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ru.sber.front.domain.model.ArXivArticleEntity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
public class ArXivHandler extends DefaultHandler {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private ArXivArticleEntity result = new ArXivArticleEntity();
    private StringBuilder data;
    private boolean entryStarted;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName) {
            case "entry" -> entryStarted = true;
            case "category" -> data = new StringBuilder(attributes.getValue("term"));
            case "link" -> {
                if ("pdf".equals(attributes.getValue("title"))) {
                    data = new StringBuilder(attributes.getValue("href"));
                }
            }
            default -> data = new StringBuilder();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (entryStarted) {
            switch (qName) {
                case "id" -> result.setArticleId(data.toString());
                case "summary" -> result.setSummary(data.toString());
                case "name" -> result.getAuthors().add(data.toString());
                case "updated" -> result.setUpdated(LocalDateTime.parse(data.toString(), formatter));
                case "published" -> result.setPublished(LocalDateTime.parse(data.toString(), formatter));
                case "title" -> result.setTitle(data.toString());
                case "arxiv:doi" -> result.setDoi(data.toString());
                case "arxiv:comment" -> result.setComment(data.toString());
                case "arxiv:journal" -> result.setJournal(data.toString());
                case "category" -> result.getCategories().add(data.toString());
                case "link" -> result.setPdfLink(data.toString());
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        data.append(new String(ch, start, length));
    }
}
