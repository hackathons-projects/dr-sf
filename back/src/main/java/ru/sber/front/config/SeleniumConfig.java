package ru.sber.front.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class SeleniumConfig {

    private static final String CHROMEDRIVER = "libs/chromedriver";

    @Bean
    public WebDriver webDriver() {
        System.setProperty("webdriver.chrome.driver", new File(CHROMEDRIVER).getAbsolutePath());
        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--no-sandbox");

        return new ChromeDriver(chromeOptions);
    }
}
