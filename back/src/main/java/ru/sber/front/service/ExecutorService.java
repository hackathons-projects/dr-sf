package ru.sber.front.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@RequiredArgsConstructor
@Slf4j
@Service
public class ExecutorService {

    private final ArXivParsingService arXivParsingService;
    private final DoePatentService doePatentService;
    private final RffiParsingService rffiParsingService;

    @PostConstruct
    public void init() {
        rffiParsingService.grabRffi();
//        doePatentService.startGrabDoePatent(1);
//        arXivParsingService.startGrabArXiv(2009, 5, 1375);
    }
}
