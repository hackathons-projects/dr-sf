package ru.sber.front.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import ru.sber.front.domain.model.DoePatentEntity;
import ru.sber.front.domain.model.DoePatentList;
import ru.sber.front.domain.model.LinkList;
import ru.sber.front.domain.repository.DoePatentRepository;

@RequiredArgsConstructor
@Slf4j
@Service
public class DoePatentService {

    private static final int PAGE_SIZE = 100;
    private static final String TEMPLATE = "https://www.osti.gov/doepatents/api/v1/records?page=%d&rows=%d";

    private final RestTemplate restTemplate = new RestTemplate();
    private final DoePatentRepository doePatentRepository;
    private final ModelMapper modelMapper;

    public void startGrabDoePatent(int startIndex) {
        int page = startIndex;
        DoePatentList doePatentEntities = parseRecords(page);
        while (!doePatentEntities.isEmpty()) {
            doePatentEntities.forEach(doePatentDto -> {
                if (!doePatentRepository.existsByOstiId(doePatentDto.getOstiId())) {
                    DoePatentEntity entity = modelMapper.map(doePatentDto, DoePatentEntity.class);
                    doePatentRepository.save(entity);
                } else {
                    log.info("Skip record " + doePatentDto.getOstiId());
                }
            });
            log.info("Page " + page + " saved");
            page++;
            doePatentEntities = parseRecords(page);
        }
        log.info("Page " + page + " saved");
    }

    private DoePatentList parseRecords(Integer page) {
        try {
            ResponseEntity<DoePatentList> forEntity = restTemplate.getForEntity(getScheme(page), DoePatentList.class);
            return forEntity.getBody();
        } catch (ResourceAccessException e) {
            log.info("Retry");
            return parseRecords(page);
        }
    }

    private String getScheme(Integer page) {
        return TEMPLATE.formatted(page, PAGE_SIZE);
    }
}
