package ru.sber.front.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;
import ru.sber.front.domain.model.RffiRecordEntity;
import ru.sber.front.domain.repository.RffiRecordRepository;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Service
public class RffiParsingService {

    private static final String RFFI_SEARCH = "http://search.rfbr.ru/";

    private final WebDriver webDriver;
    private final RffiRecordRepository rffiRecordRepository;

    public void grabRffi() {
        webDriver.get(RFFI_SEARCH);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);

        for (int contentTypeIndex = 1; contentTypeIndex < new Select(webDriver.findElement(By.id("conquest_type"))).getOptions().size(); contentTypeIndex++) {
            new Select(webDriver.findElement(By.id("conquest_type"))).selectByIndex(contentTypeIndex);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("conquest_name")));

            for (int conquestNameIndex = 1; conquestNameIndex < new Select(webDriver.findElement(By.id("conquest_name"))).getOptions().size(); conquestNameIndex++) {
                new Select(webDriver.findElement(By.id("conquest_name"))).selectByIndex(conquestNameIndex);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.id("conquest_id")));

                for (int conquestIndex = 1; conquestIndex < new Select(webDriver.findElement(By.id("conquest_id"))).getOptions().size(); conquestIndex++) {
                    new Select(webDriver.findElement(By.id("conquest_id"))).selectByIndex(conquestIndex);
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.id("main_fok_id")));


                    for (int mainFokIndex = 1; mainFokIndex < new Select(webDriver.findElement(By.id("main_fok_id"))).getOptions().size(); mainFokIndex++) {
                        new Select(webDriver.findElement(By.id("main_fok_id"))).selectByIndex(mainFokIndex);
                        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("fok_id")));

                        for (int fokIndex = 1; fokIndex < new Select(webDriver.findElement(By.id("fok_id"))).getOptions().size(); fokIndex++) {
                            boolean paginate = true;
                            new Select(webDriver.findElement(By.id("fok_id"))).selectByIndex(fokIndex);
                            waitMe();

                            do {
                                WebElement searchResult = webDriver.findElement(By.id("search_result"));
                                searchResult.findElement(By.className("bordered")).findElements(By.tagName("tr")).stream().filter(tr -> !tr.getAttribute("class").equals("tr_projects"))
                                        .forEach(tr -> {
//                                            StringBuilder sb = new StringBuilder();
                                            List<String> strings = new ArrayList<>();
                                            tr.findElements(By.tagName("td")).forEach(td -> {
//                                                sb.append(td.getText());
//                                                sb.append(", ");
                                                strings.add(td.getText());
                                            });
//                                            System.out.println(sb);
                                            rffiRecordRepository.save(new RffiRecordEntity(
                                                    strings.get(1), strings.get(2), strings.get(3), strings.get(4)
                                            ));
                                        });
                                List<WebElement> td_span_a = searchResult.findElements(By.cssSelector("td span a"));

                                if (td_span_a.size() > 0) {
                                    td_span_a.get(0).click();
                                    waitMe();
                                } else {
                                    paginate = false;
                                }
                            } while (paginate);
                        }
                    }
                }
            }
        }
    }

    private void waitMe() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
