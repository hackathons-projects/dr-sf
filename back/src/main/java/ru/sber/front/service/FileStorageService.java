package ru.sber.front.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
@Service
public class FileStorageService {

    private static final int CONNECT_TIMEOUT = 1000;
    private static final int READ_TIMEOUT = 1000;

    @Value("${filestorage}")
    private String filestorage;

    public boolean save(URL url) throws IOException {
        URL finalURL = getFinalURL(url);
        String contentType = finalURL.openConnection().getContentType();

        if ("application/pdf".equals(contentType)) {
            String[] split = finalURL.toString().split("/");
            Path pdf = Path.of(filestorage + File.separator + split[split.length - 2] + File.separator + split[split.length - 1] + ".pdf");
            Files.createDirectories(pdf.getParent());
            Files.copy(finalURL.openStream(), pdf);
            return true;
        } else {
            return false;
        }
    }

    public static URL getFinalURL(URL url) throws IOException {
       try {
           HttpURLConnection con = (HttpURLConnection) url.openConnection();
           con.setInstanceFollowRedirects(false);
           con.connect();
           con.getInputStream();

           if (con.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM || con.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP) {
               String redirectUrl = con.getHeaderField("Location");
               return getFinalURL(new URL(redirectUrl));
           }
           return url;
       } catch (SocketException socketException){
           log.info("Retry");
           return getFinalURL(url);
       }
    }
}
