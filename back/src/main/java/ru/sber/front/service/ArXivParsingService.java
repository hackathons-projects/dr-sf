package ru.sber.front.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import ru.sber.front.domain.model.ArXivArticleEntity;
import ru.sber.front.domain.repository.ArXivArticleRepository;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

@RequiredArgsConstructor
@Slf4j
@Service
public class ArXivParsingService {

    private final boolean SKIP_PDF = true;
    private final ArXivArticleRepository arXivArticleRepository;
    private final FileStorageService fileStorageService;
    private final String TEMPLATE = "http://export.arxiv.org/api/query?id_list=%s";
    private final List<String> categories = List.of(
            "astro-ph", "cond-mat", "gr-qc", "hep-ex", "hep-lat", "hep-ph", "hep-th", "math-ph", "nlin", "nucl-ex", "nucl-th", "physics", "quant-ph",
            "math", "cs", "q-bio", "q-fin", "stat", "eess", "econ"
    );
    private final RestTemplate restTemplate = new RestTemplate();
    private final ModelMapper modelMapper;

    public void startGrabArXiv(int startYear, int startMonth, int startIndex) {
        boolean init = false;
        for (int year = startYear; year < LocalDate.now().getYear(); year++) {
            for (int month = init ? 1 : startMonth; month < 12; month++) {
                grabArXiv(year, month, init ? 1 : startIndex);
                init = true;
            }
        }
    }

    private void grabArXiv(Integer year, Integer month, Integer startIndex) {
        if (year < 2007 || (year == 2007 && month < 4)) {
            categories.forEach(category -> grabOldArchive(year - 100 * (year / 100), month, category, startIndex));
        } else {
            grabNewArchive(year - 100 * (year / 100), month, startIndex);
        }
    }

    private void grabOldArchive(Integer year, Integer month, String category, Integer startIndex) {
        grabList(index -> parseOldFormat(year, month, index, category), startIndex);
    }

    private void grabNewArchive(Integer year, Integer month, Integer startIndex) {
        grabList(index -> parseNewFormat(year, month, index), startIndex);
    }

    private void grabList(Function<Integer, ArXivArticleEntity> function, Integer startIndex) {
        int index = startIndex;
        ArXivArticleEntity arXivArticleEntity = function.apply(index++);
        while (arXivArticleEntity != null) {
            if (!arXivArticleRepository.existsByArticleId(arXivArticleEntity.getArticleId())) {
                save(arXivArticleEntity);
            } else {
                log.info("Skipped: " + arXivArticleEntity.getArticleId());
            }
            arXivArticleEntity = function.apply(index++);
        }
    }

    private void save(ArXivArticleEntity arXivArticleEntity) {
        try {
            if (!SKIP_PDF) {
                boolean pdfSaved = fileStorageService.save(new URL(arXivArticleEntity.getPdfLink()));
                arXivArticleEntity.setPdfSaved(pdfSaved);
            } else {
                arXivArticleEntity.setPdfSaved(false);
            }
            arXivArticleRepository.save(arXivArticleEntity);
            log.info("Saved: " + arXivArticleEntity.getArticleId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ArXivArticleEntity parseNewFormat(Integer year, Integer month, Integer index) {
        try {
            ResponseEntity<String> forEntity = restTemplate.getForEntity(getNewScheme(year, month, index), String.class);
            return modelMapper.map(forEntity.getBody(), ArXivArticleEntity.class);
        } catch (ResourceAccessException e) {
            log.info("Retry");
            return parseNewFormat(year, month, index);
        }
    }

    private ArXivArticleEntity parseOldFormat(Integer year, Integer month, Integer index, String category) {
        try {
            ResponseEntity<String> forEntity = restTemplate.getForEntity(getOldScheme(year, month, index, category), String.class);
            return modelMapper.map(forEntity.getBody(), ArXivArticleEntity.class);
        } catch (ResourceAccessException e) {
            log.info("Retry");
            return parseOldFormat(year, month, index, category);
        }
    }

    private String getNewScheme(Integer year, Integer month, Integer index) {
        return TEMPLATE.formatted(String.format("%02d", year) + String.format("%02d", month) + "." + String.format("%04d", index));
    }

    private String getOldScheme(Integer year, Integer month, Integer index, String category) {
        return TEMPLATE.formatted(category + "/" + String.format("%02d", year) + String.format("%02d", month) + String.format("%03d", index));
    }
}
