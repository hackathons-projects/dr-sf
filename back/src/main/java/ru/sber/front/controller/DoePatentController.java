package ru.sber.front.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.front.service.DoePatentService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/arXiv")
public class DoePatentController {

    private final DoePatentService doePatentService;

    @PostMapping("/execute")
    public ResponseEntity<Void> execute(
            @RequestParam("startIndex") Integer startIndex
    ) {
        doePatentService.startGrabDoePatent(startIndex);
        return ResponseEntity.ok().build();
    }

}
