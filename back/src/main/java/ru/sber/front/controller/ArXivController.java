package ru.sber.front.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.front.service.ArXivParsingService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/arXiv")
public class ArXivController {

    private final ArXivParsingService arXivParsingService;

    @PostMapping("/execute")
    public ResponseEntity<Void> execute(
            @RequestParam("startYear") Integer startYear,
            @RequestParam("startMonth") Integer startMonth,
            @RequestParam("startIndex") Integer startIndex
    ) {
        System.out.println(startYear + " " + startMonth + " " + startIndex);
        arXivParsingService.startGrabArXiv(startYear, startMonth, startIndex);
        return ResponseEntity.ok().build();
    }
}
