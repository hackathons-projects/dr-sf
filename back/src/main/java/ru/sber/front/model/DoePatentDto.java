package ru.sber.front.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.sber.front.domain.model.AttributesList;
import ru.sber.front.domain.model.Link;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Data
public class DoePatentDto {

    @JsonProperty("osti_id")
    private String ostiId;

    private String title;

    @JsonProperty("product_type")
    private String productType;

    private String language;

    @JsonProperty("country_publication")
    private String countryPublication;

    private String description;

    @JsonProperty("publication_date")
    private Date publicationDate;

    @JsonProperty("entry_date")
    private Date entryDate;

    private String format;

    private AttributesList authors;

    @JsonProperty("doe_contract_number")
    private String doeContractNumber;

    @JsonProperty("sponsor_orgs")
    private AttributesList sponsorOrgs;

    @JsonProperty("research_orgs")
    private AttributesList researchOrgs;

    @JsonProperty("patent_number")
    private String patentNumber;

    @JsonProperty("application_number")
    private String applicationNumber;

    private String assignee;

    private List<Link> links;
}
