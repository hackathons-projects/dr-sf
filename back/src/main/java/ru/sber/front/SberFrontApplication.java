package ru.sber.front;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SberFrontApplication {

    public static void main(String[] args) {
        SpringApplication.run(SberFrontApplication.class, args);
    }

}
