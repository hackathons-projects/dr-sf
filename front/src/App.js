import './App.css';
import data from './components/data/data.json';
import ReactWordcloud from 'react-wordcloud';
import {ReactComponent as User} from './user.svg';
import {ReactComponent as Bell} from './bell.svg';
import {useState, useEffect, useCallback, useRef} from "react";
import Searchbar from "./components/searchbar";
import {ReactComponent as ArrowLeft} from './arrow-left.svg'
import {ReactComponent as ArrowRight} from './arrow-right.svg'
import {
    VictoryArea, VictoryAxis, VictoryBar, VictoryChart, VictoryErrorBar,
    VictoryGroup, VictoryLabel,
    VictoryLine, VictoryScatter,
    VictoryTheme
} from "victory";

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
}

const dict = {
    'USAP': 'Automobiles & Parts',
    'USBK': 'Banks',
    'USBM': 'Basic Materials',
    'USCH': 'Chemicals',
    'USCN': 'Construction & Materials',
    'USNC': 'Consumer Goods',
    'USCY': 'Consumer Services',
    'USFI': 'Financial Services',
    'USFN': 'Financials',
    'USFB': 'Food & Beverages',
    'USHC': 'Health Care',
    'USIG': 'Industrial Goods & Services',
    'USIN': 'Industrials',
    'USIR': 'Insurance',
    'USME': 'Media',
    'USEN': 'Oil & Gas',
    'USNG': 'Personal & Household Goods',
    'USRT': 'Retail',
    'USTC': 'Technology',
    'USTL': 'Telecommunications',
    'USCG': 'Travel & Leisure',
    'USUT': 'Utilities'
}
const datesList = []
let titles = []
let days_first = 0

function g() {
    Object.entries(data).forEach(([key1, val]) => {
        Object.entries(val).forEach(([key2, value]) => {
            if (key1 === 'USAP' && key2 === '0') {
                value.titles.forEach(title => {
                    titles.push({title: title[0]})
                })
            }
            if (datesList.some(item => {
                return item.date === value.date.slice(0, 4)
            })) {
                return
            }
            datesList.push({date: value.date.slice(0, 4)})

        });
    })
}

g();

function App() {
    const [lists, setLists] = useState([])
    const [clickedDate, setClickedDate] = useState(datesList[0].date)
    const [category, setCategory] = useState(lists[0])
    const [items, setItems] = useState([])
    const [days, setDays] = useState()
    const [valueses, setValueses] = useState(0)
    const [isSelected, setIsSelected] = useState(0)
    const [isCategoriItemSelected, setIsCategoriItemSelected] = useState(0)
    const [chartY, setChartY] = useState(0)
    const ondateclick = useCallback((date) => {
        setClickedDate(date)
    }, [])


    useEffect(() => {
        const tempItemsList = []
        const tempLists = []
        Object.entries(data).forEach(([key1, val]) => {
            Object.entries(val).forEach(([key2, values]) => {
                values.words.forEach(word => {
                    tempItemsList.push({text: word[0], value: getRandomInt(0, 100)});
                })
            });
            tempLists.push({key: key1, text: dict[key1], value: getRandomInt(0, 100)})
        });
        setItems(tempItemsList)
        setLists(tempLists)
    }, [])


    const options = {
        rotations: 2,
        rotationAngles: [0, 0],
        font: 1,
        fontSizes: [20, 50],
        enableTooltip: false

    };

    const clickCategories = (item) => {
        if (clickedDate != null) {
            const tempList = []
            setItems([])
            titles = []
            Object.entries(data[item]).forEach(([key, value]) => {
                if (value.date.slice(0, 4) === clickedDate) {
                    setDays((1000 - value.days) / 100)
                    setValueses(value.value / 500)
                    value.titles.forEach(title => {
                        titles.push({title: title[0]})
                    })
                    value.words.forEach(word => {
                        tempList.push({text: word[0], value: getRandomInt(0, 100)});
                    })
                }

            })
            setItems(tempList)
        }
    }

    const ref = useRef()


    const scroll = (scrollOffset) => {
        ref.current.scrollLeft += scrollOffset;
    };
    return (
        <div className="App">
            <header>
                <div className='title'>Фронтиры науки</div>
                <div className='user-notify'>
                    <div className='notify'><Bell/></div>
                    <div className='user'>
                        <User/>
                    </div>
                </div>
            </header>

            <Searchbar/>

            <div className="wrapper" style={{background: '#FFF'}}>
                <div className="timeline">
                    <div className='left-button' style={{position: 'relative'}} onClick={() => scroll(300)}>
                        <ArrowLeft style={{position: 'absolute', left: 10, top: 7}}/>
                    </div>


                    <div ref={ref} className="timeline-wrapper">

                        <div className="line">
                            {datesList.map((item, index) => {
                                    return (
                                        <div key={index.toString()}
                                             className={`dateList ${index === isSelected ? 'checked' : ''}`}
                                             onClick={() => {
                                                 setClickedDate(item.date)
                                                 setIsSelected(index)
                                             }}>
                                            <span>{item.date}</span>
                                        </div>
                                    )
                                }
                            )}
                        </div>
                    </div>
                    <div className='right-button' style={{position: 'relative'}} onClick={() => scroll(-300)}>
                        <ArrowRight style={{position: 'absolute', left: 12, top: 7}}/>
                    </div>
                </div>
                <div className="container">
                    <div className="categories">
                        <div className="categories__titel">
                            Направление фронтира
                        </div>
                        <div className="categories__list">
                            {
                                lists.map((item, index) => {
                                    return (
                                        <div className={`categories__item ${isCategoriItemSelected === index ? 'checked' : ''}`} key={item.key}
                                             onClick={() => {
                                                 setIsCategoriItemSelected(index)
                                                 clickCategories(item.key)
                                             }}>
                                            {item.text}
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                    <div className="wordcloud">
                        <ReactWordcloud options={options} words={items}/>
                    </div>
                </div>

                <div className="info-block">
                    <div className="graphic">
                        <svg style={{height: 0}}>
                            <defs>
                                <linearGradient id="myGradient" x1="0" x2="0" y1="0" y2="1">
                                    <stop offset="0%" stopColor="#000"/>
                                    <stop offset="100%" stopColor="transparent"/>
                                </linearGradient>
                            </defs>
                        </svg>
                        <VictoryGroup>

                            <VictoryArea
                                style={{
                                    data: {
                                        fill: "url(#myGradient)",
                                        fillOpacity: 0.7,
                                        stroke: "#000",
                                        strokeWidth: 3
                                    },
                                }}
                                animate={{
                                    duration: 3000,
                                    onLoad: {duration: 2000}
                                }}
                                interpolation="natural"
                                data={[
                                    {x: 0, y: 2 * valueses},
                                    {x: 4, y: 3 * valueses},
                                    {x: 6, y: 7 * valueses},
                                    {x: 8, y: 11 * valueses},
                                    {x: 10, y: 12 * valueses},
                                    {x: 12, y: 8 * valueses},
                                    {x: 14, y: 7 * valueses},
                                    {x: 16, y: 7 * valueses},
                                    {x: 18, y: 7 * valueses}
                                ]}
                            />
                            <VictoryBar
                                style={{
                                    data: {fill: "#000", width: 2}
                                }}
                                data={[{x: days, y: 20, y0: 0}]}

                                animate={{
                                    duration: 3000,
                                    onLoad: {duration: 2000}
                                }}
                            />

                        </VictoryGroup>
                    </div>
                    <div className="articles">
                        <div className="articles__title">
                            Популярные статьи
                        </div>
                        <div className="articles__list">
                            {titles.slice(0, 5).map((title, index) => {
                                return (<div className='articles__item' key={index}>{index + 1}. {title.title}</div>)
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
