import React from "react";
import { ReactComponent as Search } from '../search.svg';

function Searchbar() {

    return (
        <div className='searchbar'>
            <div className='field'>
                <label htmlFor="search"><Search/></label>
                <input type="text" id='search' placeholder='Проверка на фронтирность'/>
            </div>
        </div>
    )
}

export default Searchbar
